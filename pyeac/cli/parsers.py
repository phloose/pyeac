"""pyeac cli parsers setup"""
import argparse

from pyeac import __version__

common_args = argparse.ArgumentParser(add_help=False)
common_args.add_argument(
    "--debug", action="store_true", default=False, help="Toggle debug output"
)

description = (
    "Generate Equal Area Cartograms from polygon geometries of known geo formats"
)

parser = argparse.ArgumentParser(
    prog="pyeac", description=description, parents=[common_args],
)
parser.add_argument(
    "--version", action="version", version=f"{parser.prog} {__version__}"
)

# "dest" will be the the reference in the Namespace object
subparser = parser.add_subparsers(dest="cmd")

regular_desc = """Generate an equal area cartogram with a regular grid"""
regular = subparser.add_parser("reg", parents=[common_args], help=regular_desc)
regular.add_argument("--file", "-f", dest="file", required=True, help="Input file")
regular.add_argument("--out", "-o", dest="output", required=True, help="Output file")

hex_desc = """Generate an equal area cartogram with a hexagonal grid"""
hexagonal = subparser.add_parser("hex", parents=[common_args], help=hex_desc)
hexagonal.add_argument("--file", "-f", dest="file", required=True, help="Input file")
hexagonal.add_argument("--out", "-o", dest="output", required=True, help="Output file")
