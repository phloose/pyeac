"""pyeac"""
import logging

from pkg_resources import get_distribution, DistributionNotFound

from pyeac.log import userinfo, verbose

try:
    __version__ = get_distribution("pyeac").version
except DistributionNotFound:
    __version__ = "Please install package via setup.py"


logger = logging.getLogger("pyeac")
logger.setLevel(logging.INFO)
logger.addHandler(userinfo)
logger.addHandler(verbose)
