"""Utilities"""
import logging

import numpy as np
import shapely.geometry as sg

logger = logging.getLogger("pyeac.util")

__all__ = ["as_single_multipolygon", "distance_matrix", "initial_cellsize"]


def as_single_multipolygon(features):
    """Converts multiple single Polygons to one single MultiPolygon

    Args:
        features (list): List of features as GeoJSON-like mapping

    Returns:
        shapely.geometry.MultiPolygon: A shapely.geometry.MultiPolygon instance
    """
    polys = []
    for feature in features:
        # Assume that some of the geometries are invalid, so repair them as a precaution
        geom = sg.shape(feature["geometry"]).buffer(0)
        if isinstance(geom, sg.MultiPolygon):
            for single in geom:
                polys.append(single)
        else:
            polys.append(geom)
    return sg.MultiPolygon(polys)


def distance_matrix(features, grid_features):
    """Creates a distance matrix for all features centroids to all grid_features
    centroids

    Args:
        features (list): List of features as GeoJSON-like mapping
        grid_features (list): List of :class:`pyeac.grid.GridCell` instances

    Returns:
        np.array: A 2D array containing the distances
    """
    distances = []
    for feature in features:
        orig_geom_centroid = sg.shape(feature["geometry"]).centroid
        distances.append(
            [orig_geom_centroid.distance(cell.centroid) for cell in grid_features]
        )
    return np.array(distances)


def initial_cellsize(bounds):
    """Calculate the initial cellsize from bounds

    Args:
        bounds (tuple): A tuple containing coordinates of a bounding box
            (min_x, min_y, max_x, max_y)

    Returns:
        float: The calculated cellsize
    """
    min_x, min_y, max_x, max_y = bounds
    dx = max_x - min_x
    dy = max_y - min_y
    return dx / 100 if dx > dy else dy / 100
