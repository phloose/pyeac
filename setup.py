from setuptools import setup, find_packages

with open("README.md") as readme_file:
    long_description = readme_file.read()

with open("CHANGELOG.md") as changelog_file:
    changes = changelog_file.read()

setup(
    name="pyeac",
    author="Philipp Loose",
    author_email="xibalba@mailbox.org",
    url="https://gitlab.com/phloose/pyeac",
    description="tba",
    long_description=long_description + "\n\n" + changes,
    keywords="equal area cartograms, gis, geoinformatics",
    license="MIT",
    packages=find_packages(exclude=["tests", "docs"]),
    entry_points={"console_scripts": ["pyeac = pyeac.cli:main"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
    ],
    python_requires=">=3.6",
    install_requires=[
        "fiona>=1.8.13",
        "shapely>=1.7.0",
        "numpy>=1.18.1",
        "scipy>=1.4.1",
    ],
    extras_require={
        "dev": [
            "pytest>=5.1.0",
            "pytest-cov>=2.7.1",
            "pytest-mock>=1.10.4",
            "flake8>=3.7.8",
            "black>=19.3b0",
            "sphinx>=1.3",
            "sphinx-rtd-theme",
        ]
    },
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    project_urls={
        "Changelog": "https://gitlab.com/phloose/pyeac/blob/master/CHANGELOG.md",
        "Bug Tracker": "https://gitlab.com/phloose/pyeac/issues",
    },
)
