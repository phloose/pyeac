# pyEAC

**!!!WARNING: This is still in alpha stage and may not work correctly!!!**

Generate regular or hexagonal Equal Area Cartograms from polygon geometries of known geo formats
(GeoJSON, ESRI Shapefile, Geo-Package, tba).

<div>
    <img src="docs/_static/nds_reg.png" alt="Regular grid representation" width="50%">
    <img src="docs/_static/nds_hex.png" alt="Hexagonal grid representation" width="48%">
</div>

Inspired and based on [geogrid](https://github.com/jbaileyh/geogrid), a R-package for
generating equal area cartograms with regular or hexagonal grids.

-   [Motivation](#motivation)
-   [Installation & requirements](#installation-requirements)
-   [Usage](#usage)
-   [Contribution & Development](#contribution-development)
-   [Documentation](https://phloose.gitlab.io/pyeac/)

## Motivation

Choropleth maps are often used to display e.g. the results of an election. In such maps
it can happen that larger areas *seem* to have higher values because they are more
easily perceived by the viewer as a result of their size. Thus the main motivation for
generating **Equal Area Cartograms** is to produce a
representation of a spatial dataset that is not biased by the individual size of its
spatial entities.

## Installation & requirements

pyEAC requires:
- python>=3.6
- fiona>=1.8.13
- shapely>=1.7.0
- numpy>=1.18.1
- scipy>=1.4.1

To install pyEAC first clone this repository and then install the pyEAC package via pip:

```bash
git clone git@gitlab.com:phloose/pyeac.git
cd pyeac
pip install .
```

To keep things clean (and if you are using windows) it is strongly recommended to use
the conda package manager (from [Miniconda](https://docs.conda.io/en/latest/miniconda.html) or [Anaconda](https://www.anaconda.com/products/individual)) for creating a dedicated
enviroment. After cloning and cd-ing in the repository run

`conda env create -f env.yml`


This will create a new conda enviroment called `pyeac` and install the necessary
dependencies. After this has finished

`pip install .`

needs to be run to install the
package in the new conda environment.

Installation should also work when using
[venv](https://docs.python.org/3.6/tutorial/venv.html) although this has not been tested yet.

## Usage

pyEAC can be used from the command line or from within python scripts. The most easy and
fastest way of creating an **Equal Area Cartogram** for a dataset is to use the command
line.

---
**Note**

pyEAC uses [shapely](https://shapely.readthedocs.io/en/latest/manual.html) for distance
calculations, which is a planar geometry library. For best results (especially for
datasets with spherical coordinates (WGS84/EPSG:4326)) it is therefore recommended to
transform datasets into a projected coordinate system prior to usage with pyEAC.

---

### Command line interface

By using `pyeac -h` you get an overview of the cli parameters:

```bash
$ pyeac -h
usage: pyeac [-h] [--debug] [--version] {reg,hex} ...

Generate Equal Area Cartograms from polygon geometries of known geo formats

positional arguments:
  {reg,hex}
    reg       Generate an equal area cartogram with a regular grid
    hex       Generate an equal area cartogram with a hexagonal grid

optional arguments:
  -h, --help  show this help message and exit
  --debug     Toggle debug output
  --version   show program's version number and exit

```

Currently as seen above there are two subcommands to create a regular or hexagonal grid
representation of a dataset. For each of those the `-h`/`--help` argument shows more
detailed usage instructions:

```bash
$ pyeac reg -h
usage: pyeac reg [-h] [--debug] --file FILE --out OUTPUT

optional arguments:
  -h, --help            show this help message and exit
  --debug               Toggle debug output
  --file FILE, -f FILE  Input file
  --out OUTPUT, -o OUTPUT
                        Output file
```

As it follows

`pyeac reg --file dataset.shp --out dataset_reg`

or the short form

`pyeac reg -f dataset.shp -o dataset_reg`

will create an Equal Area Cartogram with regular (square) grid cells from `dataset.shp`. The output is *ALWAYS* saved in
the GeoJSON format so no additional file type specifications need to be added.

### Python scripts

pyEAC offers two functions for creating a grid of regular or hexagonal shape from
the input datasets geometries and to generate new features with the original features
properties and the new grid cell geometries.

The example below uses [fiona](https://fiona.readthedocs.io/en/latest/manual.html) to read and write datasets:

```python
import fiona
import shapely.geometry as sg

from pyeac.core import geometries_to_grid_cells, properties_to_grid_cells

with fiona.open("dataset.shp") as src:
    features = list(src)
    bounds = src.bounds
    meta = src.meta

hex_cells = geometries_to_grid_cells(features, bounds, gridtype="hex")
new_features = properties_to_grid_cells(features, hex_cells)

# geometries_to_grid_cells will convert multipolygon entities to single polygon entities
# so we need to adapt the schema.
meta["schema"]["geometry"] = "Polygon"

with fiona.open("dataset_hex.shp", "w", **meta) as out:
    for feature in new_features:
        # The hex_cells in new_features are still shapely.geometry.Polygon instances
        # which need to be mapped back
        feature["geometry"] = sg.mapping(feature["geometry"])
        out.write(feature)
```

Regardless of fiona `geometries_to_grid_cells` will work with any kind of features that
have GeoJSON-like mapping. The same applies to `properties_to_grid_cells` except that
the second parameter needs to be a list of GridCell instances (see [documentation](https://phloose.gitlab.io/pyeac/)).

## Contribution & Development

As this is still in the alpha stage ***ANY*** contribution is highly appreciated!
Be it finding bugs, enhancing documentation, development of new features or enhancement
of existing features/functionality. Please file an issue or create a merge request where
we then have a place to discuss. As a general rule for contributing code: Test early and
often - every new feature needs to have at least one test.

The easiest way to setup the development environment is to create a conda enviroment
that contains all development dependencies:

`conda env create -f env-dev.yml`

This will create a conda enviroment with name `pyeac-dev`. Then to install the package
in editable mode run:

`pip install -e .[dev]`

For testing [pytest](https://docs.pytest.org/en/latest/) is used. In order to run the
tests simply run

`pytest`

inside the repository.
