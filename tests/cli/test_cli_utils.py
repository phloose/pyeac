"""Unit tests for the pyeac.cli.utils subpackage"""
import argparse
import pytest

import pyeac.cli


class TestCmdAction:
    def dummy_func(self, **args):
        return {k: str(v) + "_called" for k, v in args.items()}

    @pytest.fixture
    def single_action(self):
        return {
            "fire": {"action": self.dummy_func, "args": {"arg1": None, "arg2": None}}
        }

    @pytest.fixture
    def more_action(self, single_action):
        single_action.update(
            {
                "other": {
                    "action": self.dummy_func,
                    "args": {"arg1": None, "arg2": None, "arg3": None},
                }
            }
        )
        return single_action

    def test_add(self):

        act = pyeac.cli.CmdAction(argparse.Namespace())
        act.add("fire", self.dummy_func, ["a", "b", "c"])

        expected = {
            "fire": {
                "action": self.dummy_func,
                "args": {"a": None, "b": None, "c": None},
            }
        }

        assert act.actions == expected

    def test_add_multiple(self):

        actions_list = [
            ("fire", self.dummy_func, ["arg1", "arg2"]),
            ("other", self.dummy_func, ["arg1", "arg2", "arg3"]),
        ]

        act = pyeac.cli.CmdAction(argparse.Namespace())
        act.add_multiple(actions_list)

        expected = {
            "fire": {"action": self.dummy_func, "args": {"arg1": None, "arg2": None}},
            "other": {
                "action": self.dummy_func,
                "args": {"arg1": None, "arg2": None, "arg3": None},
            },
        }

        assert act.actions == expected

    @pytest.mark.parametrize(
        "cmd, parsed, expected",
        [
            (
                "fire",
                dict(cmd="fire", arg1=1, arg2=2),
                dict(arg1="1_called", arg2="2_called"),
            ),
            (
                "other",
                dict(cmd="fire", arg1="a", arg2="b", arg3="c"),
                dict(arg1="a_called", arg2="b_called", arg3="c_called"),
            ),
        ],
    )
    def test_do(self, cmd, parsed, expected, more_action):

        act = pyeac.cli.CmdAction(argparse.Namespace(**parsed))

        act.actions = more_action

        assert act.do(cmd) == expected

    def test_get_parsed_arg_values(self, single_action):

        parsed = argparse.Namespace(**dict(cmd="fire", arg1=1, arg2=2))

        act = pyeac.cli.CmdAction(parsed)

        act.actions = single_action

        cmd = act.actions.get("fire")
        ret = act._get_parsed_arg_values(cmd.get("args"))

        assert ret == {"arg1": 1, "arg2": 2}
