""" Unit tests for the pyeac.util module """
import pytest
import numpy as np

import pyeac.util
from pyeac.grid import Regular


@pytest.fixture
def fake_grid(request, centroids):
    # We don't use the 5th centroid since its position is in the center of all other
    # centroids and "disturbs" the grid order. So we keep it simple and remove it.
    xs, ys = np.hsplit(np.array(centroids[:4]), 2)
    if request.param == "x_shifted":
        xs += 2
    elif request.param == "xy_shifted":
        xs += 2
        ys += 2
    return [Regular(x, y, 1) for x, y in zip(xs, ys)]


class Test_as_single_multipolygons:
    def test_multipolygon_geom_count_equals_feature_geom_count(
        self, fake_features, fake_geoms
    ):
        """
        The returned Multipolygon contains the same amount of geometries as there are
        feature geometries.
        """
        multipolygon = pyeac.util.as_single_multipolygon(fake_features)
        assert len(multipolygon) == len(fake_geoms)

    def test_multipolygon_geom_contains_feature_geoms(self, fake_features, fake_geoms):
        """
        The original features geometries are all located inside the returned single
        MultiPolygon.
        """
        multipolygon = pyeac.util.as_single_multipolygon(fake_features)
        assert all(multipolygon.contains(geom) for geom in fake_geoms)

    def test_extracts_geoms_contained_in_multipolygons_in_features(
        self, fake_features_w_multipolygons, fake_geoms_w_multipolygons
    ):
        """
        When some of the the original features geometries are MultiPolygons their single
        Polygons are extracted.
        """
        multipolygon = pyeac.util.as_single_multipolygon(fake_features_w_multipolygons)
        assert len(multipolygon) == len(fake_geoms_w_multipolygons) + 1

    def test_multipolygon_geom_contains_feature_geoms_w_multipolygons(
        self, fake_features_w_multipolygons, fake_geoms_w_multipolygons
    ):
        """
        The original features geometries which contain MultiPolygons are all located
        inside the returned single MultiPolygon.
        """
        multipolygon = pyeac.util.as_single_multipolygon(fake_features_w_multipolygons)
        assert all(multipolygon.contains(geom) for geom in fake_geoms_w_multipolygons)


@pytest.mark.parametrize(
    "fake_grid, expected",
    [
        (
            "same_position",
            [
                # fmt: off
                [0, 4, 5.66, 4],
                [4, 0, 4, 5.66],
                [5.66, 4, 0, 4],
                [4, 5.66, 4, 0]
                # fmt: on
            ],
        ),
        (
            "x_shifted",
            [
                [2, 4.47, 7.21, 6],
                [4.47, 2, 6, 7.21],
                [4.47, 2, 2, 4.47],
                [2, 4.47, 4.47, 2],
            ],
        ),
        (
            "xy_shifted",
            [
                [2.83, 6.32, 8.49, 6.32],
                [2.83, 2.83, 6.32, 6.32],
                [2.83, 2.83, 2.83, 2.83],
                [2.83, 6.32, 6.32, 2.83],
            ],
        ),
    ],
    indirect=["fake_grid"],
)
def test_distance_matrix(fake_grid, expected, fake_features):
    """
    distance matrix creates a NxN matrix of distances from all original feature to all
    new grid cells, where N is the count of features in the original dataset.
    """
    # To be in line with the fake grid w.r.t feature count (distance matrix has shape
    # NxN) we remove the 5th feature
    distance_matrix = pyeac.util.distance_matrix(fake_features[:4], fake_grid)
    np.testing.assert_almost_equal(distance_matrix, expected, decimal=2)


@pytest.mark.parametrize(
    "bounds, expected",
    [((0, 0, 5, 5), 0.05), ((0, 0, 10, 5), 0.1), ((0, 0, 5, 10), 0.1)],
    ids=lambda val: str(val) if (isinstance(val, tuple)) else val,
)
def test_initial_cellsize(bounds, expected):
    """initial_cellsize returns 1/100 of the largest extent in x or y direction."""
    assert pyeac.util.initial_cellsize(bounds) == expected
