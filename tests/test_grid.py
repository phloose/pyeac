""" Unit tests for the pyeac.grid module """
import pytest
import numpy as np
import shapely.geometry as sg

import pyeac.grid

regular_grids = [
    pytest.param(
        (0, 0, 5, 5),
        1,
        # fmt: off
        [
            [0, 0], [1, 0], [2, 0], [3, 0], [4, 0],
            [0, 1], [1, 1], [2, 1], [3, 1], [4, 1],
            [0, 2], [1, 2], [2, 2], [3, 2], [4, 2],
            [0, 3], [1, 3], [2, 3], [3, 3], [4, 3],
            [0, 4], [1, 4], [2, 4], [3, 4], [4, 4],
        ],
        # fmt: on
        id="bounds=(0, 0, 5, 5)-cellsize=1",
    ),
    pytest.param(
        (0, 0, 5, 5),
        2,
        # fmt: off
        [
            [0, 0], [2, 0], [4, 0],
            [0, 2], [2, 2], [4, 2],
            [0, 4], [2, 4], [4, 4]
        ],
        # fmt: on
        id="bounds=(0, 0, 5, 5)-cellsize=2",
    ),
    pytest.param(
        (-5, -5, 0, 0),
        1,
        # fmt: off
        [
            [-5, -5], [-4, -5], [-3, -5], [-2, -5], [-1, -5],
            [-5, -4], [-4, -4], [-3, -4], [-2, -4], [-1, -4],
            [-5, -3], [-4, -3], [-3, -3], [-2, -3], [-1, -3],
            [-5, -2], [-4, -2], [-3, -2], [-2, -2], [-1, -2],
            [-5, -1], [-4, -1], [-3, -1], [-2, -1], [-1, -1],
        ],
        # fmt: on
        id="bounds=(-5, -5, 0, 0)-cellsize=1",
    ),
]

hex_grids = [
    pytest.param(
        (0, 0, 5, 5),
        1,
        # fmt: off
        [
            [0, 0],
            [0.86, 0],
            [1.73, 0],
            [2.59, 0],
            [3.46, 0],
            [4.33, 0],

            [0.43, 0.75],
            [1.29, 0.75],
            [2.16, 0.75],
            [3.03, 0.75],
            [3.89, 0.75],
            [4.76, 0.75],

            [0, 1.5],
            [0.86, 1.5],
            [1.73, 1.5],
            [2.59, 1.5],
            [3.46, 1.5],
            [4.33, 1.5],

            [0.43, 2.25],
            [1.29, 2.25],
            [2.16, 2.25],
            [3.03, 2.25],
            [3.89, 2.25],
            [4.76, 2.25],

            [0, 3.0],
            [0.86, 3.0],
            [1.73, 3.0],
            [2.59, 3.0],
            [3.46, 3.0],
            [4.33, 3.0],

            [0.43, 3.75],
            [1.29, 3.75],
            [2.16, 3.75],
            [3.03, 3.75],
            [3.89, 3.75],
            [4.76, 3.75],

            [0, 4.5],
            [0.86, 4.5],
            [1.73, 4.5],
            [2.59, 4.5],
            [3.46, 4.5],
            [4.33, 4.5],
        ],
        # fmt: on
        id="bounds=(0, 0, 5, 5)-cellsize=1",
    ),
    pytest.param(
        (0, 0, 5, 5),
        2,
        # fmt: off
        [
            [0.0, 0.0],
            [1.73, 0.0],
            [3.46, 0.0],

            [0.86, 1.5],
            [2.59, 1.5],
            [4.33, 1.5],

            [0.0, 3.0],
            [1.73, 3.0],
            [3.46, 3.0],

            [0.86, 4.5],
            [2.59, 4.5],
            [4.33, 4.5],
        ],
        # fmt: on
        id="bounds=(0, 0, 5, 5)-cellsize=2",
    ),
    pytest.param(
        (-5, -5, 0, 0),
        2,
        # fmt: off
        [
            [-5, -5],
            [-3.26, -5],
            [-1.53, -5],

            [-4.13, -3.5],
            [-2.40, -3.5],
            [-0.66, -3.5],

            [-5, -2],
            [-3.26, -2],
            [-1.53, -2],

            [-4.13, -0.5],
            [-2.40, -0.5],
            [-0.66, -0.5],
        ],
        # fmt: on
        id="bounds=(-5, -5, 0, 0)-cellsize=2",
    ),
]


class Test_RegularGrid:
    @pytest.mark.parametrize("bounds, cellsize, expected", regular_grids)
    def test_regular_grid_coords(self, bounds, cellsize, expected):
        """
        An instance of pyeac.grid.RegularGrid has a coordinate sequence (list of x-y
        coordinates) according to the given bounds and cellsize that represents the
        grid.
        """
        reggrid = pyeac.grid.RegularGrid(bounds=bounds, cellsize=cellsize)
        np.testing.assert_equal(reggrid.coords, expected)


class Test_HexGrid:
    @pytest.mark.parametrize("bounds, cellsize, expected", hex_grids)
    def test_hex_grid_coords(self, bounds, cellsize, expected):
        """
        An instance of pyeac.grid.HexGrid has a coordinate sequence (list of x-y
        coordinates) according to the given bounds and cellsize that represents the
        grid.
        """
        hexgrid = pyeac.grid.HexGrid(bounds=bounds, cellsize=cellsize)
        np.testing.assert_almost_equal(hexgrid.coords, expected, decimal=2)


hexagonal_cells = [
    pytest.param(
        0,
        0,
        1,
        [
            (0.43, 0.25),
            (0.0, 0.5),
            (-0.43, 0.25),
            (-0.43, -0.25),
            (0.0, -0.5),
            (0.43, -0.25),
            (0.43, 0.25),
        ],
        id="x=0-y=0-size=1",
    ),
    pytest.param(
        0,
        0,
        2,
        [
            (0.87, 0.5),
            (0.0, 1.0),
            (-0.87, 0.5),
            (-0.87, -0.5),
            (0.0, -1.0),
            (0.87, -0.5),
            (0.87, 0.5),
        ],
        id="x=0-y=0-size=2",
    ),
    pytest.param(
        10,
        54,
        1,
        [
            (10.43, 54.25),
            (10.0, 54.5),
            (9.56, 54.25),
            (9.56, 53.75),
            (10.0, 53.5),
            (10.43, 53.75),
            (10.43, 54.25),
        ],
        id="x=10-y=54-size=1",
    ),
]

square_cells = [
    pytest.param(
        0,
        0,
        1,
        [  # fmt: off
            (0.5, 0.5),
            (-0.5, 0.5),
            (-0.5, -0.5),
            (0.5, -0.5),
            (0.5, 0.5),
        ],  # fmt: on
        id="x=0-y=0-size=1",
    ),
    pytest.param(
        0,
        0,
        2,
        [  # fmt: off
            (1.0, 1.0),
            (-1.0, 1.0),
            (-1.0, -1.0),
            (1.0, -1.0),
            (1.0, 1.0),
        ],  # fmt: on
        id="x=0-y=0-size=2",
    ),
    pytest.param(
        10,
        54,
        1,
        [  # fmt: off
            (10.5, 54.5),
            (9.5, 54.5),
            (9.5, 53.5),
            (10.5, 53.5),
            (10.5, 54.5),
        ],  # fmt: on
        id="x=10-y=54-size=1",
    ),
]


class Test_Hexagon:
    def test_is_shapely_geometry_polygon(self):
        """pyeac.grid.Hexagon creates a shapely.geometry.Polygon instance"""
        assert isinstance(pyeac.grid.Hexagon(0, 0, 1), sg.Polygon)

    @pytest.mark.parametrize("x, y, size, expected", hexagonal_cells)
    def test_hexagonal_cell_coords(self, x, y, size, expected):
        """
        An instance of pyeac.grid.Hexagon creates a shapely.geometry.Polygon instance
        with hexagonal shape of a certain size around a center point x, y.
        """
        hex_cell = pyeac.grid.Hexagon(x, y, size)
        np.testing.assert_almost_equal(hex_cell.exterior.coords, expected, decimal=2)


class Test_Regular:
    def test_is_shapely_geometry_polygon(self):
        """pyeac.grid.Regular creates a shapely.geometry.Polygon instance"""
        assert isinstance(pyeac.grid.Regular(0, 0, 1), sg.Polygon)

    @pytest.mark.parametrize("x, y, size, expected", square_cells)
    def test_square_cell_coords(self, x, y, size, expected):
        """
        An instance of pyeac.grid.Regular creates a shapely.geometry.Polygon instance
        with square shape of a certain size around a center point x, y.
        """
        square_cell = pyeac.grid.Regular(x, y, size)
        np.testing.assert_almost_equal(square_cell.exterior.coords, expected, decimal=2)


def test_GridCell_fails_to_instantiate():
    """pyeac.grid.GridCell is only a base class and never instantiated."""
    with pytest.raises(NotImplementedError):
        pyeac.grid.GridCell(0, 0, 1)
