# Changelog

## [0.1.0-a1] - 06.05.2020

### Added

- Core components:
  - `geometries_to_grid_cells`: Function to create a regular or hexagonal grid
    representation of feature geometries
  - `properties_to_grid_cells`: Function to create new features from original features
    and corresponding grid cells
  - `Grid`: Base class for creating grid coordinates from given bounds and cellsize.
  - `HexGrid`: Class for creating hexagonal grid coordinates
  - `RegularGrid`: Class for creating regular grid coordinates
  - `GridCell`: Base class for creating grid cell polygons around a center point with a
    given size
  - `Hexagon`: Class for creating hexagonal polygons
  - `Regular`: Class for creating square polygons
- Helper functions: `distance_matrix`, `as_single_multipolygon`, `initial_cellsize`
- CLI endpoint `pyeac` with subcommands `reg` and `hex`
- README with motivation, installation instructions, usage and development &
  contribution section
- Documentation using sphinx
- Unit tests for the core functionality
