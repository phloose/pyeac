Installation
============

.. Give detailed instructions on how to install the package.

pyEAC requires:

* python>=3.6
* fiona>=1.8.13
* shapely>=1.7.0
* numpy>=1.18.1
* scipy>=1.4.1

To install pyEAC first clone this repository and then install the pyEAC package via pip:

.. code-block:: bash

    git clone git@gitlab.com:phloose/pyeac.git
    cd pyeac
    pip install .


To keep things clean (and if you are using windows) it is strongly recommended to use
the conda package manager (from `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ or `Anaconda <https://www.anaconda.com/products/individual>`_ ) for creating a dedicated
enviroment. After cloning and cd-ing in the repository run

.. code-block:: bash

    conda env create -f env.yml


This will create a new conda enviroment called `pyeac` and install the necessary
dependencies. After this has finished

.. code-block:: bash

    pip install .

needs to be run to install the
package in the new conda environment.

Installation should also work when using `venv <https://docs.python.org/3.6/tutorial/venv.html>`_  although this has not been tested yet.
