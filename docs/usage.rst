Getting started
===============

.. Describe in simple terms how to use the functionality of the package.
.. Also provide information about the command line interface.

pyEAC can be used from the command line or from within python scripts. The most easy and
fastest way of creating an **Equal Area Cartogram** for a dataset is to use the command
line.

.. note:: pyEAC uses `shapely <https://shapely.readthedocs.io/en/latest/manual.html>`_
    for distance calculations, which is a planar geometry library. For best results (especially for
    datasets with spherical coordinates (WGS84/EPSG:4326)) it is therefore recommended to
    transform datasets into a projected coordinate system prior to usage with pyEAC.


Command line interface
++++++++++++++++++++++

By using :code:`pyeac -h` you get an overview of the cli parameters:

.. code-block::

    $ pyeac -h
    usage: pyeac [-h] [--debug] [--version] {reg,hex} ...

    Generate Equal Area Cartograms from polygon geometries of known geo formats

    positional arguments:
    {reg,hex}
        reg       Generate an equal area cartogram with a regular grid
        hex       Generate an equal area cartogram with a hexagonal grid

    optional arguments:
    -h, --help  show this help message and exit
    --debug     Toggle debug output
    --version   show program's version number and exit

Currently as seen above there are two subcommands to create a regular or hexagonal grid
representation of a dataset. For each of those the :code:`-h`/:code:`--help` argument shows more
detailed usage instructions:

.. code-block::

    $ pyeac reg -h
    usage: pyeac reg [-h] [--debug] --file FILE --out OUTPUT

    optional arguments:
    -h, --help            show this help message and exit
    --debug               Toggle debug output
    --file FILE, -f FILE  Input file
    --out OUTPUT, -o OUTPUT
                            Output file

As it follows

.. code-block:: bash

    pyeac reg --file dataset.shp --out dataset_reg


or the short form

.. code-block:: bash

    pyeac reg -f dataset.shp -o dataset_reg

will create an Equal Area Cartogram with regular (square) grid cells from :code:`dataset.shp`. The output is *ALWAYS* saved in
the GeoJSON format so no additional file type specifications need to be added.

Python scripts
++++++++++++++

pyEAC offers two functions for creating a grid of regular or hexagonal shape from
the input datasets geometries and to generate new features with the original features
properties and the new grid cell geometries.

The example below uses `fiona <https://fiona.readthedocs.io/en/latest/manual.html>`_ to read and write datasets:

.. code-block:: python

    import fiona
    import shapely.geometry as sg

    from pyeac.core import geometries_to_grid_cells, properties_to_grid_cells

    with fiona.open("dataset.shp") as src:
        features = list(src)
        bounds = src.bounds
        meta = src.meta

    hex_cells = geometries_to_grid_cells(features, bounds, gridtype="hex")
    new_features = properties_to_grid_cells(features, hex_cells)

    # geometries_to_grid_cells will convert multipolygon entities to single polygon entities
    # so we need to adapt the schema.
    meta["schema"]["geometry"] = "Polygon"

    with fiona.open("dataset_hex.shp", "w", **meta) as out:
        for feature in new_features:
            # The hex_cells in new_features are still shapely.geometry.Polygon instances
            # which need to be mapped back
            feature["geometry"] = sg.mapping(feature["geometry"])
            out.write(feature)


Regardless of fiona :code:`geometries_to_grid_cells` will work with any kind of features that
have GeoJSON-like mapping. The same applies to :code:`properties_to_grid_cells` except that
the second parameter needs to be a list of GridCell instances (see documentation).
